﻿using UnityEngine;
using Hive;
using Hive.InAppPurchases;

public class AppDelegate : AppDelegateBase
{
    protected override void OnLaunch()
    {
#if UNITY_STANDALONE && !UNITY_EDITOR
            Screen.SetResolution(480, 852, false);
#endif
        // set 60 FPS by default
        Application.targetFrameRate = 60;
        Input.multiTouchEnabled = true;

        // Subscribe to app life cycle events
        HiveFoundation.Instance.Launch += HiveFoundation_Launch;
        HiveFoundation.Instance.Pause += HiveFoundation_Pause;
        HiveFoundation.Instance.Resume += HiveFoundation_Resume;

        //InApp
        var hiveInAppProducts = new InAppProductsCollection
        {
            HiveInAppProducts.Test0,
            HiveInAppProducts.Test1,
            HiveInAppProducts.Subscription0
        };

        var hive = HiveFoundation.CreateHost()
            .AddInAppPurchase( hiveInAppProducts );

        hive.BuildAndRun();
    }

    private void HiveFoundation_Launch()
    {
    }

    private void HiveFoundation_Resume()
    {
    }

    private void HiveFoundation_Pause()
    {
    }
}