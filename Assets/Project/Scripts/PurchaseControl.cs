﻿using Hive.InAppPurchases;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseControl : MonoBehaviour
{
    [SerializeField]
    private Text _log;

    public void PurchaseTest0()
    {
        InAppPurchases.PurchaseProduct( HiveInAppProducts.Test0, ( sender, args ) => Log( "Test0", args ) );
    }

    public void PurchaseTest1()
    {
        InAppPurchases.PurchaseProduct( HiveInAppProducts.Test1, ( sender, args ) => Log( "Test1", args ) );
    }

    public void PurchaseSubscription0()
    {
        InAppPurchases.PurchaseProduct( HiveInAppProducts.Subscription0, ( sender, args ) => Log( "Subscription0", args ) );
    }

    private void Log( string label, InAppEventArgs args )
    {
        _log.text += string.Format( "\n{0}:\n{1}\n", label, args );
    }

    public void RestorePurchases()
    {
        InAppPurchases.RestorePurchases( ( sender, args ) => 
        {
            _log.text += "\nRestore:";
            foreach( var product in args.Products )
            {
                _log.text += string.Format( "\n{0}", product.Identifier );
            }
        } );
    }
}
