﻿using Hive.InAppPurchases;


public static class HiveInAppProducts
{
    //public static InAppProductsCollection GetCollection()
    //{
    //    return new InAppProductsCollection
    //    {
    //        SubscriptionFree,
    //        SubscriptionMonth,
    //        SubscriptionYear,

    //        SubscriptionFreeDiscount,
    //        SubscriptionMonthDiscount,
    //        SubscriptionYearDiscount,
    //    };
    //}

    //private static InAppProduct _subscriptionFree;
    //public static InAppProduct SubscriptionFree { get { return _subscriptionFree ?? (_subscriptionFree = CreateInAppSubscription( "premium.free" )); } }

    //private static InAppProduct _subscriptionMonth;
    //public static InAppProduct SubscriptionMonth { get { return _subscriptionMonth ?? (_subscriptionMonth = CreateInAppSubscription( "premium.month" )); } }

    //private static InAppProduct _subscriptionYear;
    //public static InAppProduct SubscriptionYear { get { return _subscriptionYear ?? (_subscriptionYear = CreateInAppSubscription( "premium.year" )); } }

    //private static InAppProduct _subscriptionYearDiscount;
    //public static InAppProduct SubscriptionYearDiscount { get { return _subscriptionYearDiscount ?? (_subscriptionYearDiscount = CreateInAppSubscription( "premium.year.discount" )); } }

    //private static InAppProduct _subscriptionMonthDiscount;
    //public static InAppProduct SubscriptionMonthDiscount { get { return _subscriptionMonthDiscount ?? (_subscriptionMonthDiscount = CreateInAppSubscription( "premium.month.discount" )); } }


    private static InAppProduct _test0;
    public static InAppProduct Test0 { get { return _test0 ?? (_test0 = new InAppProduct( GetIdentifier( "test0" ), InAppProductType.Consumable )); } }

    private static InAppProduct _test1;
    public static InAppProduct Test1 { get { return _test1 ?? (_test1 = new InAppProduct( GetIdentifier( "test1" ), InAppProductType.NonConsumable )); } }
    
    private static InAppProduct _subscription0;
    public static InAppProduct Subscription0 { get { return _subscription0 ?? (_subscription0 = CreateInAppSubscription( "subscription0" )); } }

    private static InAppProduct CreateInAppSubscription( string postfix )
    {
        string identifier = GetIdentifier( postfix );
        return new InAppProduct( identifier, InAppProductType.AutoRenewableSubscription );
    }

    private static string GetIdentifier( string postfix )
    {
        return string.Format( "{0}.iap.{1}", AppProfile.Instance.BundleId, postfix );
    }
}
